CREATE DATABASE recetas;
USE recetas;

CREATE TABLE usuario(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nick VARCHAR(20) UNIQUE NOT NULL,
	password VARCHAR(20) NOT NULL,
	nombre VARCHAR(20) NOT NULL,
	apellido1 VARCHAR(20),
	apellido2 VARCHAR(20),
	edad TINYINT UNSIGNED,
	email VARCHAR(60) UNIQUE NOT NULL,
	telefono VARCHAR(12),
	fecha_registro DATE NOT NULL
);
CREATE TABLE admin(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nick VARCHAR(20) UNIQUE NOT NULL,
	password VARCHAR(20) NOT NULL,
	fecha_registro DATE NOT NULL
);
CREATE TABLE tipo_receta(
	id TINYINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(30) NOT NULL
);
CREATE TABLE receta(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	titulo VARCHAR(50) UNIQUE NOT NULL,
	elaboracion TEXT NOT NULL,
	fecha_publicacion DATE DEFAULT CURRENT_DATE(),
	id_tipo_receta TINYINT UNSIGNED,
	id_admin INT UNSIGNED,
	FOREIGN KEY (id_tipo_receta) REFERENCES tipo_receta(id),
	FOREIGN KEY (id_admin) REFERENCES admin(id)
);
CREATE TABLE tipo_ingrediente(
	id TINYINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(30) NOT NULL
);
CREATE TABLE ingrediente(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(30) UNIQUE NOT NULL,
	id_tipo_ingrediente TINYINT UNSIGNED,
	FOREIGN KEY (id_tipo_ingrediente) REFERENCES tipo_ingrediente(id)
);
CREATE TABLE ingrediente_receta(
	id_ingrediente INT UNSIGNED,
	id_receta INT UNSIGNED,
	cantidad VARCHAR(20),
	FOREIGN KEY (id_ingrediente) REFERENCES ingrediente(id),
	FOREIGN KEY (id_receta) REFERENCES receta(id) ON DELETE CASCADE,
	PRIMARY KEY (id_ingrediente, id_receta)
);
CREATE TABLE comentario(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	fecha_hora_publicacion DATETIME DEFAULT CURRENT_TIMESTAMP(),
	contenido TINYTEXT NOT NULL,
	id_usuario INT UNSIGNED,
	id_receta INT UNSIGNED,
	FOREIGN KEY (id_usuario) REFERENCES usuario(id) ON DELETE CASCADE,
	FOREIGN KEY (id_receta) REFERENCES receta(id) ON DELETE CASCADE
);
CREATE TABLE valoracion(
	id_usuario INT UNSIGNED,
	id_receta INT UNSIGNED,
	nota INT NOT NULL,
	FOREIGN KEY (id_usuario) REFERENCES usuario(id) ON DELETE CASCADE,
	FOREIGN KEY (id_receta) REFERENCES receta(id) ON DELETE CASCADE,
	PRIMARY KEY (id_usuario, id_receta)
);
CREATE TABLE mensaje(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	asunto VARCHAR(40) NOT NULL,
	contenido TINYTEXT NOT NULL,
	leido TINYINT(0) DEFAULT 0,
	fecha_hora_enviado DATETIME DEFAULT CURRENT_TIMESTAMP(),
	id_usuario INT UNSIGNED,
	FOREIGN KEY (id_usuario) REFERENCES usuario(id) ON DELETE CASCADE
);



