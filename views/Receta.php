<?php

declare(strict_types=1);

namespace Views;

class Receta extends View
{
    private \Models\Receta $modelo;

    public function __construct(\Models\Receta $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        if ($this->modelo->getAccion()) {

            $msj = 'Gracias por tu opinión!';
            header("Location: " . URL_BASE . "receta/" . $this->modelo->getDatos()['id_receta'] . "/?e=0&m=$msj");
            exit;
        } elseif (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "?e=1&m=$msj");
            exit;
        } else {

            $titulo = 'Receta';
            ob_start();
            $receta = $this->modelo->getDatos();
?>
            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col">
                        <div class="card h-100">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col">
                                        Tipo de receta: <?= $receta['tipo_receta'] ?>
                                    </div>
                                    <div class="col">
                                        <i><small>Añadida el <?= $receta['fecha_publicacion'] ?> por <strong><?= $receta['admin'] ?></strong></small></i>
                                    </div>
                                </div>

                                <h2 class="card-title mb-3"><?= $receta['titulo'] ?></h2>

                                <!-- Fotografía -->
                                <div id="ingredientes" class="row mb-3">
                                    <div class="col">
                                        <?php if (!empty($receta['ingredientes'])) : ?>
                                            <h6>Ingredientes:</h6>
                                            <ul>
                                            <?php
                                            foreach ($receta['ingredientes'] as $ingrediente) :
                                                echo '<li>' . $ingrediente['nombre']
                                                    . (!empty($ingrediente['cantidad']) ? (', ' . $ingrediente['cantidad']) : '')
                                                    . ', <small><i>' . $ingrediente['tipo_ingrediente'] . '</i></small></li>';
                                            endforeach;
                                            echo '</ul>';
                                        else :
                                            null;
                                        endif; ?>
                                    </div>
                                </div>

                                <div id="elaboracion" class="row mb-3">
                                    <div class="col"><?= $receta['elaboracion'] ?></div>
                                </div>

                                <div id="valoracion" class="row">
                                    <div class="col">
                                        <?php if (!empty($receta['datos_valoracion'])) : ?>
                                            Nota media: <?= round((float) $receta['datos_valoracion']['nota_media'], 1) ?>
                                            <br>
                                            Número de valoraciones: <?= $receta['datos_valoracion']['numero_valoraciones'] ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col">
                                        <?php if (isset($_SESSION['login_usuario']) && $_SESSION['login_usuario'] === true) : ?>
                                            <form action="<?= URL_BASE ?>receta/valorar/<?= $receta['id'] ?>/" method="POST">
                                                <label for="nota">Valorar:</label>
                                                <select name="nota" id="nota">
                                                    <?php for ($i = 1; $i <= 5; $i++) :
                                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                                    endfor; ?>
                                                </select>
                                                <input type="submit" value="Enviar">
                                            </form>
                                        <?php endif; ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="comentarios" class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <div class="card-body">
                                <?php if (isset($_SESSION['login_usuario']) && $_SESSION['login_usuario'] === true) : ?>
                                    <div id="nuevo-comentario" class="row mb-3">
                                        <div class="col">
                                            <form action="<?= URL_BASE ?>receta/comentar/<?= $receta['id'] ?>/" method="POST" id="form_login">
                                                <div class="row mb-3">
                                                    <label for="input_comentario" class="form-label">Nuevo Comentario</label>
                                                    <input type="text" name="contenido" class="form-control" id="input_comentario" required>
                                                </div>
                                                <button type="submit" class="btn btn-primary">Comentar</button>
                                            </form>
                                        </div>
                                    </div>

                                    <div id="lista-comentarios" class="row">
                                        <div class="col">
                                            <?php if (!empty($receta['comentarios'])) :
                                                echo '<p>Hay ' . count($receta['comentarios']) . ' comentario' . (count($receta['comentarios']) > 1 ? 's' : '') . '</p>';
                                                echo '<ol class="list-group list-group-numbered">';
                                                foreach ($receta['comentarios'] as $comentario) : ?>
                                                    <li id="comentario_<?= $comentario['id_comentario'] ?>" class="list-group-item d-flex justify-content-between align-items-start">
                                                        <div class=" ms-2 me-auto">
                                                            <div class="fw-bold">
                                                                <?= $comentario['usuario'] ?>
                                                            </div>
                                                            <?= $comentario['contenido'] ?>
                                                        </div>
                                                        <?= $comentario['fecha_hora_publicacion'] ?>
                                                    </li>
                                                <?php endforeach; ?>
                                                </ol>
                                            <?php else : ?>
                                                <p class="card-text">No hay comentarios.</p>
                                            <?php endif; ?>
                                        <?php else : ?>
                                            <p class="card-text">Necesitas iniciar sesión para ver los comentarios.</p>
                                        <?php endif; ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
