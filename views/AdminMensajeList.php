<?php

declare(strict_types=1);

namespace Views;

class AdminMensajeList extends AdminView
{
    private \Models\Mensaje $modelo;

    public function __construct(\Models\Mensaje $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        if ($this->modelo->getAccion()) {

            header("Location: " . URL_BASE . "mensaje/lista/");
            exit;
        } else {
            $titulo = 'Bandeja de entrada';
            ob_start();
?>
            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <div class="card-body">
                                <h2 class="card-title">Bandeja de entrada</h2>

                                <ul class="list-group">
                                    <?php
                                    foreach ($this->modelo->getDatos() as $mensaje) {
                                        $li = '<li class="list-group-item"><a href="' . URL_BASE . 'mensaje/' . $mensaje['id'] . '/">';
                                        $li .= ($mensaje['leido'] == false) ? '<span class="badge bg-secondary">Nuevo</span> ' : '';
                                        $li .= $mensaje['asunto'] . '</a>, <i><small>enviado a las ' . $mensaje['fecha_hora_enviado'] . ' por <strong>' . $mensaje['nick'];
                                        $li .= '</strong></small></i><a href="' . URL_BASE . 'mensaje/eliminar/' . $mensaje['id'] . '/"><i class="bi bi-trash"></i></a></li>';
                                        echo $li;
                                    }
                                    ?>
                                </ul>

                            </div>
                            <div class="card-footer">
                                <ul class="pagination">
                                    <li class="page-item<?php echo ($this->modelo->getPagina() == 1) ? ' disabled' : '' ?>">
                                        <a class="page-link" href="?pagina=<?php echo $this->modelo->getPagina() - 1 ?>">
                                            &laquo;
                                        </a>
                                    </li>

                                    <?php for ($i = 1; $i <= $this->modelo->getNumeroPaginas(); $i++) : ?>
                                        <li class="page-item<?php echo ($this->modelo->getPagina() == $i) ? ' active' : '' ?>">
                                            <a class='page-link' href='?pagina=<?= $i ?>'>
                                                <?= $i ?>
                                            </a>
                                        </li>
                                    <?php endfor; ?>

                                    <li class="page-item<?php echo ($this->modelo->getPagina() == $this->modelo->getNumeroPaginas()) ? ' disabled' : '' ?>">
                                        <a class="page-link" href="?pagina=<?php echo $this->modelo->getPagina() + 1 ?>">
                                            &raquo;
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
