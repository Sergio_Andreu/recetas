<?php

declare(strict_types=1);

namespace Views;

class Home extends View
{
    private \Models\Receta $modelo;

    public function __construct(\Models\Receta $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render()
    {
        $titulo = 'Home';
        ob_start();
?>
        <div class="container px-4 px-lg-5">
            <div class="row align-items-center my-5">
                <div class="col">
                    <h1 class="font-weight-light">Bienvenido a RPV</h1>
                </div>
            </div>
            <div class="row">
                <div class="col mb-5">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title">Disfrute de nuestras recetas</h2>

                            <ul class="list-group">
                                <?php foreach ($this->modelo->getDatos() as $receta) : ?>
                                    <li class="list-group-item">
                                        <a href="<?= URL_BASE ?>receta/<?= $receta['id'] ?>/"><?= $receta['titulo'] ?></a>
                                        <i><small>añadida el <?= $receta['fecha_publicacion'] ?> por <strong><?= $receta['admin'] ?></strong></small></i>
                                    </li>
                                <?php endforeach; ?>
                            </ul>

                        </div>
                        <div class="card-footer">
                            <ul class="pagination">
                                <li class="page-item<?php echo ($this->modelo->getPagina() == 1) ? ' disabled' : '' ?>">
                                    <a class="page-link" href="?pagina=<?php echo $this->modelo->getPagina() - 1 ?>">
                                        &laquo;
                                    </a>
                                </li>

                                <?php for ($i = 1; $i <= $this->modelo->getNumeroPaginas(); $i++) : ?>
                                    <li class="page-item<?php echo ($this->modelo->getPagina() == $i) ? ' active' : '' ?>">
                                        <a class='page-link' href='?pagina=<?= $i ?>'>
                                            <?= $i ?>
                                        </a>
                                    </li>
                                <?php endfor; ?>

                                <li class="page-item<?php echo ($this->modelo->getPagina() == $this->modelo->getNumeroPaginas()) ? ' disabled' : '' ?>">
                                    <a class="page-link" href="?pagina=<?php echo $this->modelo->getPagina() + 1 ?>">
                                        &raquo;
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
        $contenido = ob_get_clean();

        ob_start();
        $this->plantilla($titulo, $contenido);
        $html = ob_get_clean();

        return $html;
    }
}
