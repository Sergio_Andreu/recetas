<?php

declare(strict_types=1);

namespace Views;

class RegistroForm extends View
{
    private \Models\Usuario $modelo;

    public function __construct(\Models\Usuario $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render()
    {
        if ($this->modelo->getAccion()) {

            $msj = 'Registro realizado. Ya puedes loguearte';
            header("Location: " . URL_BASE . "login/?e=0&m=$msj");
            exit;
        } elseif (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "registro/?e=1&m=$msj");
            exit;
        } else {

            $titulo = 'Registro';
            ob_start();
?>
            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <form action="<?= URL_BASE ?>registro/realizar/" method="POST" id="form_registro">

                                <div class="card-body">
                                    <h2 class="card-title">Registro</h2>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="input_nick" class="form-label">Nickname&nbsp;<span class="text-danger">*</span></label>
                                            <input type="text" name="nick" class="form-control" id="input_nick" required minlength="3" placeholder="Nombre de usuario, usado para el login">
                                        </div>
                                        <div class="col">
                                            <label for="input_password" class="form-label">Contraseña&nbsp;<span class="text-danger">*</span></label>
                                            <input type="password" name="password" class="form-control" id="input_password" required minlength="6" placeholder="Mínimo 6 carácteres alfanuméricos">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="input_nombre" class="form-label">Nombre&nbsp;<span class="text-danger">*</span></label>
                                            <input type="text" name="nombre" class="form-control" id="input_nombre" required placeholder="Nombre">
                                        </div>
                                        <div class="col">
                                            <label for="input_email" class="form-label">Email&nbsp;<span class="text-danger">*</span></label>
                                            <input type="email" name="email" class="form-control" id="input_email" required placeholder="Dirección de correo electrónico">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="input_apellido1" class="form-label">Apellido 1</label>
                                            <input type="text" name="apellido1" class="form-control" id="input_apellido1" placeholder="Primer apellido">
                                        </div>
                                        <div class="col">
                                            <label for="input_apellido2" class="form-label">Apellido 2</label>
                                            <input type="text" name="apellido2" class="form-control" id="input_apellido2" placeholder="Segundo apellido">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="input_edad" class="form-label">Edad&nbsp;<span class="text-danger">*</span></label>
                                            <input type="number" name="edad" class="form-control" id="input_edad" value="1" min="1" max="200" required placeholder="Edad">
                                        </div>
                                        <div class="col">
                                            <label for="input_telefono" class="form-label">Teléfono</label>
                                            <input type="text" name="telefono" class="form-control" id="input_telefono" placeholder="Teléfono">
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary">Enviar</button>
                                        </div>
                                        <div class="col">
                                            <span class="text-danger">* Obligatorios</span>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
