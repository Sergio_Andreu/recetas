<?php

declare(strict_types=1);

namespace Views;

class PerfilForm extends View
{
    private \Models\Usuario $modelo;

    public function __construct(\Models\Usuario $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render()
    {
        if ($this->modelo->getAccion()) {

            $msj = 'Perfil actualizado';
            header("Location: " . URL_BASE . "usuario/" . $_SESSION['id_usuario'] . "/?e=0&m=$msj"); // podría pillarlo de modelo
            exit;
        } elseif (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "usuario/" . $_SESSION['id_usuario'] . "/?e=1&m=$msj");
            // esta redirección 
            exit;
        } else {

            $titulo = 'Perfil';
            ob_start();
            $usuario = $this->modelo->getDatos();
?>
            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col">
                        <div class="card h-100">
                            <form action="<?= URL_BASE ?>usuario/actualizar/<?= $usuario['id'] ?>/" method="POST" id="form_perfil">

                                <div class="card-body">
                                    <h2 class="card-title">Perfil</h2>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="input_nick" class="form-label">Nickname&nbsp;<span class="text-danger">*</span></label>
                                            <input type="text" name="nick" class="form-control" id="input_nick" required minlength="3" value="<?= $usuario['nick'] ?>">
                                            <input type="hidden" name="nick_actual" id="nick_actual" required value="<?= $usuario['nick'] ?>">
                                        </div>
                                        <div class="col">
                                            <p>Registrado el <?= $usuario['fecha_registro'] ?></p>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="input_nombre" class="form-label">Nombre&nbsp;<span class="text-danger">*</span></label>
                                            <input type="text" name="nombre" class="form-control" id="input_nombre" required value="<?= $usuario['nombre'] ?>">
                                        </div>
                                        <div class="col">
                                            <label for="input_email" class="form-label">Email&nbsp;<span class="text-danger">*</span></label>
                                            <input type="email" name="email" class="form-control" id="input_email" required value="<?= $usuario['email'] ?>">
                                            <input type="hidden" name="email_actual" id="email_actual" required value="<?= $usuario['email'] ?>">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="input_apellido1" class="form-label">Apellido 1</label>
                                            <input type="text" name="apellido1" class="form-control" id="input_apellido1" value="<?= $usuario['apellido1'] ?>">
                                        </div>
                                        <div class="col">
                                            <label for="input_apellido2" class="form-label">Apellido 2</label>
                                            <input type="text" name="apellido2" class="form-control" id="input_apellido2" value="<?= $usuario['apellido2'] ?>">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="input_edad" class="form-label">Edad</label>
                                            <input type="number" name="edad" class="form-control" id="input_edad" min="1" max="200" required value="<?= $usuario['edad'] ?>">
                                        </div>
                                        <div class="col">
                                            <label for="input_telefono" class="form-label">Teléfono</label>
                                            <input type="text" name="telefono" class="form-control" id="input_telefono" value="<?= $usuario['telefono'] ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary">Actualizar datos</button>
                                        </div>
                                        <div class="col">
                                            <span class="text-danger">* Obligatorios</span>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#modal_cambiar_password">Cambiar contraseña</button>
                                    </div>
                                    <div class="col">
                                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modal_eliminar_cuenta">Eliminar cuenta</button>
                                    </div>
                                </div>
                                <div class="modal fade" id="modal_cambiar_password" tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modal_cambiar_password_titulo">Cambiar contraseña</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="<?= URL_BASE ?>usuario/cambiar-pass/<?= $usuario['id'] ?>/" method="POST">
                                                    <div class="row mb-1">
                                                        <label for="pass_actual">Contraseña actual:</label>
                                                        <input type="password" name="pass_actual" id="pass_actual" class="form-control" required>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label for="pass_nueva">Nueva contraseña:</label>
                                                        <input type="password" name="pass_nueva" id="pass_nueva" class="form-control" required minlength="<?= MIN_PASSWORD_LENGTH ?>">
                                                    </div>
                                                    <div class="row mb-1">
                                                        <label for="pass_confirm">Confirme la contraseña:</label>
                                                        <input type="password" name="pass_confirm" id="pass_confirm" class="form-control" required minlength="<?= MIN_PASSWORD_LENGTH ?>">
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Volver</button>
                                                        </div>
                                                        <div class="col">
                                                            <button type="submit" class="btn btn-primary">Guardar cambio</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="modal_eliminar_cuenta" tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modal_eliminar_cuenta_titulo">Eliminar cuenta</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="<?= URL_BASE ?>usuario/baja/<?= $usuario['id'] ?>/" method="POST">
                                                    <div class="row mb-1">
                                                        <label for="pass_actual">Contraseña actual:</label>
                                                        <input type="password" name="pass_actual" id="pass_actual" class="form-control" required>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Volver</button>
                                                        </div>
                                                        <div class="col">
                                                            <button type="submit" class="btn btn-danger">Eliminar cuenta</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
