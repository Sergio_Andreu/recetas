<?php

declare(strict_types=1);

namespace Views;

class LoginForm extends View
{
    private \Models\Autenticable $modelo;

    public function __construct(\Models\Autenticable $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        if ($this->modelo->getLogin()) {

            header('Location: ' . URL_BASE);
            exit;
        } else {

            if ($this->modelo->getIntentado()) {

                $msj = 'Usuario y/o contraseña incorrectos';
                header("Location: " . URL_BASE . "login/?e=1&m=$msj");
                exit;
            } else {
                $titulo = 'Login';
                ob_start();
?>
                <div class="container px-4 px-lg-5">
                    <div class="row my-5">
                        <div class="col mb-5">
                            <div class="card h-100">
                                <form action="<?= URL_BASE ?>login/autenticar/" method="POST">

                                    <div class="card-body">
                                        <h2 class="card-title">Login</h2>
                                        <div class="row mb-3">
                                            <label for="input_nick" class="form-label">Nickname</label>
                                            <input type="text" name="nick" class="form-control" id="input_nick" required placeholder="Nick del usuario">
                                        </div>
                                        <div class="row mb-3">
                                            <label for="input_password" class="form-label">Contraseña</label>
                                            <input type="password" name="password" class="form-control" id="input_password" required placeholder="Contraseña">
                                        </div>
                                    </div>

                                    <div class="card-footer"><button type="submit" class="btn btn-primary">Login</button></div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

<?php
                $contenido = ob_get_clean();

                ob_start();
                $this->plantilla($titulo, $contenido);
                $html = ob_get_clean();

                return $html;
            }
        }
    }
}
