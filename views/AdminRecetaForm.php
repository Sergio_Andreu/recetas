<?php

declare(strict_types=1);

namespace Views;

class AdminRecetaForm extends AdminView
{
    private \Models\Receta $modelo;

    public function __construct(\Models\Receta $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        if ($this->modelo->getAccion()) {

            $msj = 'Receta guardada.';
            // header("Location: " . URL_BASE . "receta/" . $this->modelo->getDatos()['id'] ? ('editar/' . $this->modelo->getDatos()['id']) : '' . "/?e=0&m=$msj");
            // mandaré de vuelta a la lista
            header("Location: " . URL_BASE . "receta/lista/?e=0&m=$msj"); // aunque en la lista no hay código para mostrar la notificación
            exit;
        } elseif (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "receta/lista/?e=1&m=$msj");
            exit;
        } else {

            $titulo = 'Receta';
            ob_start();
            $receta = $this->modelo->getDatos();
?>
            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <form action="<?= URL_BASE ?>receta/<?php echo !empty($this->modelo->getDatos()['id']) ? ('actualizar/' . $this->modelo->getDatos()['id'] . '/') : 'crear/'; ?>" method="post" id="form_receta" name="form_receta">

                                <div class="card-body">
                                    <h2 class="card-title">Crear/Editar receta</h2>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="titulo" class="form-label">Título</label>
                                            <input type="text" name="titulo" class="form-control" id="titulo" required value="<?= $receta['titulo'] ?? '' ?>">
                                            <?php
                                            if (!empty($receta['titulo'])) {
                                                echo '<input type="hidden" name="titulo_actual" id="titulo_actual" required value="' . $receta['titulo'] . '">';
                                            }
                                            ?>
                                        </div>
                                        <div class="col">
                                            <label for="tipo_receta" class="form-label">Tipo</label>
                                            <select name="tipo_receta" id="tipo_receta" class="form-control">
                                                <?php
                                                foreach ($this->modelo->getTiposRecetas() as $tipo) {
                                                    echo '<option value="' . $tipo['id'] . '"'
                                                        . (!empty($receta['id_tipo_receta']) ? ($receta['id_tipo_receta'] == $tipo['id'] ? ' selected' : '') : '')
                                                        . '>' . $tipo['nombre'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="select_ingredientes" class="form-label">Ingredientes</label>
                                            <select id="select_ingredientes" class="form-control">
                                                <?php
                                                foreach ($this->modelo->getIngredientesRecetas() as $tipo) {
                                                    echo '<option value="' . $tipo['id'] . '">' . $tipo['nombre'] . ' (' . $tipo['tipo_ingrediente'] . ')</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-sm" id="btn_anadir_ingrediente">Añadir ingrediente</button>
                                                <button type="button" class="btn btn-warning btn-sm" id="btn_quitar_ingrediente">Quitar ingrediente</button>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label for="lista_ingredientes" class="form-label">Ingredientes utilizados por la receta</label>
                                            <input type="hidden" name="ingredientes" value=''>;
                                            <ul name="lista_ingredientes" class="list-group overflow-auto" id="lista_ingredientes" style="max-height: 15rem;">
                                                <?php
                                                if (!empty($receta['ingredientes'])) {
                                                    foreach ($receta['ingredientes'] as $ingrediente) {
                                                        echo '<li class="list-group-item list-group-item-action" id="' . $ingrediente['id_ingrediente'] . '">'
                                                            . $ingrediente['nombre'] . ' (' . $ingrediente['tipo_ingrediente'] . ')</li>';
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="input_receta_elaboracion" class="form-label">Elaboración</label>
                                        <textarea id="input_receta_elaboracion" name="elaboracion" cols="30" rows="10"><?= $receta['elaboracion'] ?? '' ?></textarea>
                                    </div>
                                </div>

                                <div class="card-footer"><button type="submit" class="btn btn-primary">Enviar</button></div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                // Inicialización
                // Recorro la lista de ingredientes de la receta y los quito del select
                // aprovecho y les meto evento click
                document.querySelector('#lista_ingredientes').querySelectorAll('li').forEach(function(elemento) {
                    let id_ingrediente = elemento.attributes['id'].value;
                    document.querySelector('#select_ingredientes').querySelector('option[value="' + id_ingrediente + '"]').remove();
                    elemento.addEventListener('click', clickLista);
                });
                // añadir eventos a los botones
                document.getElementById('btn_anadir_ingrediente').addEventListener('click', anadirIngrediente);
                document.getElementById('btn_quitar_ingrediente').addEventListener('click', quitarIngrediente);
                // funciones de añadir y quitar elementos
                function anadirIngrediente() {
                    let id_ingrediente = document.querySelector('#select_ingredientes').value;
                    let texto_ingrediente = document.querySelector('#select_ingredientes').querySelector('option[value="' + id_ingrediente + '"]').textContent
                    // eliminar opción del select
                    document.querySelector('#select_ingredientes').querySelector('option[value="' + id_ingrediente + '"]').remove();
                    // añadir a la lista, al final mismamente
                    let li = document.createElement('li');
                    let texto = document.createTextNode(texto_ingrediente);
                    li.id = id_ingrediente;
                    li.className = 'list-group-item list-group-item-action';
                    li.appendChild(texto);
                    li.addEventListener('click', clickLista);
                    document.querySelector('#lista_ingredientes').appendChild(li);
                }

                function quitarIngrediente() {
                    let id_ingrediente = document.querySelector('#lista_ingredientes').querySelector('.active').id;
                    let texto_ingrediente = document.querySelector('#lista_ingredientes').querySelector('.active').textContent;
                    document.querySelector('#lista_ingredientes').querySelector('.active').remove();
                    let opcion = document.createElement('option');
                    let texto = document.createTextNode(texto_ingrediente);
                    opcion.value = id_ingrediente;
                    opcion.appendChild(texto);
                    document.querySelector('#select_ingredientes').appendChild(opcion);
                }

                // función que controla activar el elemento clicado de la lista y desactiva los demás
                function clickLista() {
                    document.querySelector('#lista_ingredientes').querySelectorAll('li').forEach(function(elemento) {
                        elemento.classList.remove('active');
                    });
                    this.classList.add('active');
                }

                document.form_receta.addEventListener('submit', prepararForm);

                function prepararForm() {
                    // coger los ingredientes de la lista y meter sus ids en un campo para postear
                    let string_ids = '';
                    document.querySelector('#lista_ingredientes').querySelectorAll('li').forEach(function(elemento) {
                        string_ids += elemento.id + ',';
                    });
                    console.log('submit: ' + string_ids);
                    document.form_receta.ingredientes.value = string_ids;
                }
            </script>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
