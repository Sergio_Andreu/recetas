<?php

declare(strict_types=1);

namespace Views;

class AdminMensaje extends AdminView
{
    private \Models\Mensaje $modelo;

    public function __construct(\Models\Mensaje $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        if ($this->modelo->getAccion()) {

            header("Location: " . URL_BASE . "mensaje/lista/");
            exit;
        } elseif (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "mensaje/lista/?e=1&m=$msj");
            exit;
        } else {

            $titulo = 'Mensaje';
            ob_start();
            $mensaje = $this->modelo->getDatos();
?>
            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <form action="<?= URL_BASE ?>admin/crear/" method="POST">

                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col">
                                            <i><small>Enviado a las <?= $mensaje['fecha_hora_enviado'] ?> por <strong><?= $mensaje['nick'] ?></strong></small></i>
                                        </div>
                                    </div>
                                    <h2 class="card-title mb-3">Asunto: <?= $mensaje['asunto'] ?></h2>
                                    <div id="contenido" class="row mb-3">
                                        <div class="col">
                                            <h6>Contenido:</h6>
                                            <?= $mensaje['contenido'] ?>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
