<?php

declare(strict_types=1);

namespace Views;

class AdminUsuarioList extends AdminView
{
    private \Models\Usuario $modelo;

    public function __construct(\Models\Usuario $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        if ($this->modelo->getAccion()) {

            header("Location: " . URL_BASE . "usuario/lista/");
            exit;
        } else {
            $titulo = 'Usuarios registrados';
            ob_start();
?>
            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <div class="card-body">
                                <h2 class="card-title">Usuarios registrados</h2>

                                <ul class="list-group">
                                    <?php
                                    foreach ($this->modelo->getDatos() as $usuario) {
                                        echo '<li class="list-group-item">' . $usuario['nick'] . '<a href="' . URL_BASE . 'usuario/eliminar/' . $usuario['id'] . '/"><i class="bi bi-trash"></i></a></li>';
                                    }
                                    ?>
                                </ul>

                            </div>
                            <div class="card-footer">
                                <ul class="pagination">
                                    <li class="page-item<?php echo ($this->modelo->getPagina() == 1) ? ' disabled' : '' ?>">
                                        <a class="page-link" href="?pagina=<?php echo $this->modelo->getPagina() - 1 ?>">
                                            &laquo;
                                        </a>
                                    </li>

                                    <?php for ($i = 1; $i <= $this->modelo->getNumeroPaginas(); $i++) : ?>
                                        <li class="page-item<?php echo ($this->modelo->getPagina() == $i) ? ' active' : '' ?>">
                                            <a class='page-link' href='?pagina=<?= $i ?>'>
                                                <?= $i ?>
                                            </a>
                                        </li>
                                    <?php endfor; ?>

                                    <li class="page-item<?php echo ($this->modelo->getPagina() == $this->modelo->getNumeroPaginas()) ? ' disabled' : '' ?>">
                                        <a class="page-link" href="?pagina=<?php echo $this->modelo->getPagina() + 1 ?>">
                                            &raquo;
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
