<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404 Página no encontrada - RPV</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="<?= URL_BASE ?>assets/favicon.ico" />
    <!-- Estilos -->
    <link rel="stylesheet" href="<?= URL_BASE ?>css/styles.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container px-5">
            <a class="navbar-brand" href="<?= URL_BASE ?>">RPV</a>
        </div>
    </nav>
    <div class="container px-4 px-lg-5">
        <div class="row my-5">
            <div class="col mb-5">
                <div class="card h-100">
                    <div class="card-body">
                        <h2 class="card-title">Error 404</h2>
                        <p class="card-text">La página que buscas no existe.</p>
                    </div>
                    <div class="card-footer"><a href="<?= URL_BASE ?>" class="btn btn-primary">Ir a página de inicio</a></div>
                </div>
            </div>
        </div>
    </div>
    <footer class="py-5 bg-dark">
        <div class="container px-4 px-lg-5">
            <p class="m-0 text-center text-white">RPV - <?php echo date('Y'); ?></p>
        </div>
    </footer>
</body>

</html>