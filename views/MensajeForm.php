<?php

declare(strict_types=1);

namespace Views;

class MensajeForm extends View
{
    private \Models\Mensaje $modelo;

    public function __construct(\Models\Mensaje $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render()
    {
        if ($this->modelo->getAccion()) {

            $msj = 'Mensaje enviado. Tus sugerencias nos ayudan a crecer, ¡gracias!';
            header("Location: " . URL_BASE . "mensaje/?e=0&m=$msj");
            exit;
        } elseif (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "mensaje/?e=1&m=$msj");
            exit;
        } else {

            $titulo = 'Enviar Mensaje';
            ob_start();
?>
            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <form action="<?= URL_BASE ?>mensaje/enviar/" method="POST" id="form_mensaje">

                                <div class="card-body">
                                    <h2 class="card-title">Escribir Mensaje</h2>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <label for="input_asunto" class="form-label">Asunto</label>
                                            <input type="text" name="asunto" class="form-control" id="input_asunto" required>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="input_contenido" class="form-label">Mensaje</label>
                                        <textarea id="input_contenido" name="contenido" cols="30" rows="10" required></textarea>
                                    </div>
                                </div>
                                
                                <div class="card-footer"><button type="submit" class="btn btn-primary">Enviar</button></div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
