<?php

declare(strict_types=1);

namespace Views;

class AdminRegistroForm extends AdminView
{
    private \Models\Admin $modelo;

    public function __construct(\Models\Admin $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render()
    {
        if ($this->modelo->getAccion()) {

            $msj = 'Admin creado.';
            header("Location: " . URL_BASE . "admin/nuevo/?e=0&m=$msj");
            exit;
        } elseif (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "admin/nuevo/?e=1&m=$msj");
            exit;
        } else {

            $titulo = 'Registro';
            ob_start();
?>

            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <form action="<?= URL_BASE ?>admin/crear/" method="POST">

                                <div class="card-body">
                                    <h2 class="card-title">Crear admin</h2>
                                    <div class="row mb-3">
                                        <label for="input_nick" class="form-label">Nickname</label>
                                        <input type="text" name="nick" class="form-control" id="input_nick" required minlength="3" placeholder="Nombre de admin, usado para el login">
                                    </div>
                                    <div class="row mb-3">
                                        <label for="input_password" class="form-label">Contraseña</label>
                                        <input type="password" name="password" class="form-control" id="input_password" required minlength="6" placeholder="Mínimo 6 carácteres alfanuméricos">
                                    </div>
                                </div>

                                <div class="card-footer"><button type="submit" class="btn btn-primary">Crear</button></div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
