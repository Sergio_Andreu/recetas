<?php

$rutas = [
    # Expresión Regular URI                 Auth         Modelo                Vista                          Controlador                   Método Cont.    Offset parámetros URI
    ['/^\/login\/$/',                       'nologin',   '\Models\Login',      '\Views\LoginForm',            null,                         null,           null],
    ['/^\/login\/autenticar\/$/',           'nologin',   '\Models\Login',      '\Views\LoginForm',            '\Controllers\Login',         'autenticar',   null],
    ['/^\/cerrar-sesion\/$/',               'user',      '\Models\Login',      '\Views\LoginForm',            '\Controllers\Login',         'cerrarSesion', null],
    ['/^\/registro\/$/',                    'nologin',   '\Models\Usuario',    '\Views\RegistroForm',         null,                         null,           null],
    ['/^\/registro\/realizar\/$/',          'nologin',   '\Models\Usuario',    '\Views\RegistroForm',         '\Controllers\Crud',          'create',       null],
    ['/^\/usuario\/\d+\/$/',                'user',      '\Models\Usuario',    '\Views\PerfilForm',           '\Controllers\Crud',          'read',         1],
    ['/^\/usuario\/actualizar\/\d+\/$/',    'user',      '\Models\Usuario',    '\Views\PerfilForm',           '\Controllers\Crud',          'update',       2],
    ['/^\/usuario\/cambiar-pass\/\d+\/$/',  'user',      '\Models\Usuario',    '\Views\PerfilForm',           '\Controllers\User',          'cambiarPass',  2],
    ['/^\/usuario\/baja\/\d+\/$/',          'user',      '\Models\Usuario',    '\Views\PerfilForm',           '\Controllers\User',          'baja',         2],
    ['/^\/$/',                              'nologin',   '\Models\Receta',     '\Views\Home',                 '\Controllers\Lista',         'lista',        null],
    ['/^\/receta\/\d+\/$/',                 'nologin',   '\Models\Receta',     '\Views\Receta',               '\Controllers\Crud',          'read',         1],
    ['/^\/receta\/valorar\/\d+\/$/',        'user',      '\Models\Receta',     '\Views\Receta',               '\Controllers\Receta',        'valorar',      2],
    ['/^\/receta\/comentar\/\d+\/$/',       'user',      '\Models\Receta',     '\Views\Receta',               '\Controllers\Receta',        'comentar',     2],
    ['/^\/mensaje\/$/',                     'user',      '\Models\Mensaje',    '\Views\MensajeForm',          null,                         null,           null],
    ['/^\/mensaje\/enviar\/$/',             'user',      '\Models\Mensaje',    '\Views\MensajeForm',          '\Controllers\Crud',          'create',       null],
    # Admin:
    ['/^\/admin\/login\/$/',                'nologin',   '\Models\AdminLogin', '\Views\AdminLoginForm',       null,                         null,           null],
    ['/^\/admin\/login\/autenticar\/$/',    'nologin',   '\Models\AdminLogin', '\Views\AdminLoginForm',       '\Controllers\Login',         'autenticar',   null],
    ['/^\/admin\/cerrar-sesion\/$/',        'admin',     '\Models\AdminLogin', '\Views\AdminLoginForm',       '\Controllers\Login',         'cerrarSesion', null],
    ['/^\/admin\/nuevo\/$/',                'admin',     '\Models\Admin',      '\Views\AdminRegistroForm',    null,                         null,           null],
    ['/^\/admin\/crear\/$/',                'admin',     '\Models\Admin',      '\Views\AdminRegistroForm',    '\Controllers\Crud',          'create',       null],
    ['/^\/usuario\/lista\/$/',              'admin',     '\Models\Usuario',    '\Views\AdminUsuarioList',     '\Controllers\Lista',         'lista',        null],
    ['/^\/usuario\/eliminar\/\d+\/$/',      'admin',     '\Models\Usuario',    '\Views\AdminUsuarioList',     '\Controllers\Crud',          'delete',       2],
    ['/^\/mensaje\/lista\/$/',              'admin',     '\Models\Mensaje',    '\Views\AdminMensajeList',     '\Controllers\Lista',         'lista',        null],
    ['/^\/mensaje\/\d+\/$/',                'admin',     '\Models\Mensaje',    '\Views\AdminMensaje',         '\Controllers\Crud',          'read',         1],
    ['/^\/mensaje\/eliminar\/\d+\/$/',      'admin',     '\Models\Mensaje',    '\Views\AdminMensajeList',     '\Controllers\Crud',          'delete',       2],
    ['/^\/receta\/lista\/$/',               'admin',     '\Models\Receta',     '\Views\AdminRecetaList',      '\Controllers\Lista',         'lista',        null],
    ['/^\/receta\/$/',                      'admin',     '\Models\Receta',     '\Views\AdminRecetaForm',      null,                         null,           null],
    ['/^\/receta\/crear\/$/',               'admin',     '\Models\Receta',     '\Views\AdminRecetaForm',      '\Controllers\Crud',          'create',       null],
    ['/^\/receta\/editar\/\d+\/$/',         'admin',     '\Models\Receta',     '\Views\AdminRecetaForm',      '\Controllers\Crud',          'read',         2],
    ['/^\/receta\/actualizar\/\d+\/$/',     'admin',     '\Models\Receta',     '\Views\AdminRecetaForm',      '\Controllers\Crud',          'update',       2],
    ['/^\/receta\/eliminar\/\d+\/$/',       'admin',     '\Models\Receta',     '\Views\AdminRecetaList',      '\Controllers\Crud',          'delete',       2]
];

return $rutas;
