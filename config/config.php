<?php

declare(strict_types=1);
error_reporting(E_ALL);
// Mostrar errores en test
ini_set('display_errors', '1');
// Logear errores en prod, y no mostrar
// ini_set('display_errors', 0);
// ini_set('log_errors', 1);


// CONSTANTES
// Base de datos
define('DB_HOST', '127.0.0.1');
define('DB_PUERTO', '13306');
define('DB_USUARIO', 'sergio');
define('DB_PASSWORD', 'debian1121');
define('DB_NOMBRE', 'recetas');
define('DB_CHARSET', 'utf8mb4');

// Requisitos login
define('MIN_USERNAME_LENGTH', 3);
define('MIN_PASSWORD_LENGTH', 6);
define('MAX_EDAD', 200);

// Constantes servidor
define('DIR_RAIZ', dirname(dirname(__FILE__))); // C:\ ... \recetas
define('URL_BASE', 'http://practica.recetas.es/');
