<?php

declare(strict_types=1);

namespace Core;

class Ruta
{
    private string $regex_uri;
    private string $nivel_auth;
    private string $modelo;
    private string $vista;
    private ?string $controlador;
    private ?string $metodo;
    private ?int $offset_parametros;

    public function __construct(string $regex_uri, string $nivel_auth,string $modelo, string $vista, ?string $controlador, ?string $metodo, ?int $offset_parametros)
    {
        $this->regex_uri = $regex_uri;
        $this->nivel_auth = $nivel_auth;
        $this->modelo = $modelo;
        $this->vista = $vista;
        $this->controlador = $controlador;
        $this->metodo = $metodo;
        $this->offset_parametros = $offset_parametros;
    }

    public function uriEncaja(string $uri): bool
    {
        if (preg_match($this->regex_uri, $uri) === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getRegex(): string
    {
        return $this->regex_uri;
    }

    public function getNivelAuth(): string
    {
        return $this->nivel_auth;
    }

    public function getModelo(): string
    {
        return $this->modelo;
    }

    public function getVista(): string
    {
        return $this->vista;
    }

    public function getControlador(): ?string
    {
        return $this->controlador;
    }

    public function getMetodo(): ?string
    {
        return $this->metodo;
    }

    public function getOffsetParametros(): ?int
    {
        return $this->offset_parametros;
    }
}
