<?php

declare(strict_types=1);

namespace Core;

class Despachador
{
    private array $rutas;

    public function __construct()
    {
        $this->rutas = [];
    }

    public function cargarRutas(array $definiciones_rutas): void
    {
        foreach ($definiciones_rutas as $def_ruta) {
            $ruta = new Ruta($def_ruta[0], $def_ruta[1], $def_ruta[2], $def_ruta[3], $def_ruta[4], $def_ruta[5], $def_ruta[6]);
            $this->rutas[] = $ruta;
        }
    }

    public function resolver(Request $request)
    {
        $request_uri = $request->getUri();

        if (!str_ends_with($request_uri, '/')) {
            $request_uri .= '/';
        }

        try {
            $hay_match = false;
            foreach ($this->rutas as $ruta) {
                if ($ruta->uriEncaja($request_uri)) {
                    $hay_match = true;
                    $ruta_coincidente = $ruta;

                    break;
                }
            }
            if ($hay_match && $this->sesionAutorizadaParaRuta($ruta_coincidente)) {

                $nombre_modelo = $ruta_coincidente->getModelo();
                $nombre_vista = $ruta_coincidente->getVista();
                $nombre_controlador = $ruta_coincidente->getControlador();
                $metodo = $ruta_coincidente->getMetodo();

                $parametros_uri = $this->extraerParametros($request, $ruta_coincidente);
                $request->setParametrosUri($parametros_uri);

                $db = new DB();
                $modelo = new $nombre_modelo($db);

                if (!is_null($nombre_controlador) && !is_null($metodo)) {
                    $controlador = new $nombre_controlador($modelo);
                    $controlador->$metodo($request);
                }
                $vista = new $nombre_vista($modelo);

                return $vista->render();
            } else {
                throw new \Exception('Ruta no válida');
            }
        } catch (\Exception $e) {
            if ($e->getMessage() === 'Ruta no válida') {
                http_response_code(404);
                include DIR_RAIZ . '/vistas/404.php';
                exit;
            } else {
                throw $e;
            }
        }
    }

    private function sesionAutorizadaParaRuta(Ruta $ruta)
    {
        $logins_sesion = ['nologin'];
        if (isset($_SESSION['login_usuario']) && $_SESSION['login_usuario'] === true && !empty($_SESSION['id_usuario'])) {
            $logins_sesion[] = 'user';
        }
        if (isset($_SESSION['login_admin']) && $_SESSION['login_admin'] === true && !empty($_SESSION['id_admin'])) {
            $logins_sesion[] = 'admin';
        }
        if (in_array($ruta->getNivelAuth(), $logins_sesion, true)) {
            return true;
        } else {
            return false;
        }
    }

    private function extraerParametros(Request $request, Ruta $ruta): array
    {
        // Esta función en verdad no me gusta aquí.
        // Creo que acabará como Request::setParametrosUri(Ruta $ruta_coincidente)
        $parametros_uri = [];
        if ($ruta->getOffsetParametros()) {
            $array_uri = explode('/', $request->getUri());
            $parametros_uri = array_slice($array_uri, $ruta->getOffsetParametros() + 1);
        }
        return $parametros_uri;
    }
}
