<?php

declare(strict_types=1);

namespace Controllers;

class Crud
{
    private \Models\Crud $modelo;

    public function __construct(\Models\Crud $modelo)
    {
        $this->modelo = $modelo;
    }

    public function create(\Core\Request $request): void
    {
        $this->modelo->create($request->getPost());
    }

    public function read(\Core\Request $request): void
    {
        $this->modelo->read((int) $request->getParametrosUri()[0]);
    }

    public function update(\Core\Request $request): void
    {
        $this->modelo->update((int) $request->getParametrosUri()[0], $request->getPost());
    }

    public function delete(\Core\Request $request): void
    {
        $this->modelo->delete((int) $request->getParametrosUri()[0]);
    }
}
