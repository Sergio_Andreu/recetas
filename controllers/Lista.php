<?php

declare(strict_types=1);

namespace Controllers;

class Lista
{
    private \Models\Listable $modelo;

    public function __construct(\Models\Listable $modelo)
    {
        $this->modelo = $modelo;
    }

    public function lista(\Core\Request $request)
    {
        $pagina = isset($request->getGet()['pagina']) ? (int) $request->getGet()['pagina'] : 1;
        $this->modelo->getLista($pagina);
    }
}
