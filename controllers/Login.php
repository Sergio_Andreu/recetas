<?php

declare(strict_types=1);

namespace Controllers;

class Login
{
    private \Models\Autenticable $modelo;

    public function __construct(\Models\Autenticable $modelo)
    {
        $this->modelo = $modelo;
    }

    public function autenticar(\Core\Request $request): void
    {
        $this->modelo->setLogin($request->getPost());
    }

    public function cerrarSesion(): void
    {
        $this->modelo->endLogin();
    }
}
