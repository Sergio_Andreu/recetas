<?php

declare(strict_types=1);

namespace Controllers;

class Usuario
{   # Controlador extra para un par de funciones exclusivas de usuario
    private \Models\Usuario $modelo;

    public function __construct(\Models\Usuario $modelo)
    {
        $this->modelo = $modelo;
    }

    public function cambiarPass(\Core\Request $request): void
    {
        $this->modelo->cambiarPassword((int) $request->getParametrosUri()[0], $request->getPost());
    }

    public function baja(\Core\Request $request): void
    {
        $this->modelo->baja((int) $request->getParametrosUri()[0], $request->getPost());
    }
}
