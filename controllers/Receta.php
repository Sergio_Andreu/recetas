<?php

declare(strict_types=1);

namespace Controllers;

class Receta
{   # Controlador extra para un par de funciones exclusivas de receta
    private \Models\Receta $modelo;

    public function __construct(\Models\Receta $modelo)
    {
        $this->modelo = $modelo;
    }

    public function comentar(\Core\Request $request): void
    {
        $this->modelo->comentar((int) $request->getParametrosUri()[0], $request->getPost());
    }

    public function valorar(\Core\Request $request): void
    {
        $this->modelo->valorar((int) $request->getParametrosUri()[0], $request->getPost());
    }
}
