<?php

namespace Models;

interface Crud
{
    public function create(array $datos): void;
    public function read(int $id): void;
    public function update(int $id, array $datos): void;
    public function delete(int $id): void;
}
