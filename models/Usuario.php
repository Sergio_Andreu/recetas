<?php

declare(strict_types=1);

namespace Models;

class Usuario implements CRUD, Listable
{
    private \Core\DB $db;
    private array $datos;
    private bool $accion_realizada;
    private array $errores;
    private int $items_pagina;

    public function __construct(\Core\DB $db)
    {
        $this->db = $db;
        $this->datos = [];
        $this->accion_realizada = false;
        $this->errores = [];
        $this->items_pagina = 20;
    }

    # CRUD
    public function create(array $datos): void
    {
        $this->errores = $this->validar(
            $datos['nick'],
            '',
            $datos['password'],
            $datos['nombre'],
            $datos['apellido1'],
            $datos['apellido2'],
            (int) $datos['edad'],
            $datos['email'],
            '',
            $datos['telefono']
        );

        if (empty($this->errores)) {
            $this->createDB(
                $datos['nick'],
                $datos['password'],
                $datos['nombre'],
                $datos['apellido1'],
                $datos['apellido2'],
                (int) $datos['edad'],
                $datos['email'],
                $datos['telefono']
            );
        }
    }

    public function read(int $id): void
    {
        if (!$this->existeId($id)) {
            $this->errores = ['El \'ID\' no corresponde a ningún usuario'];
        } elseif ($id !== $_SESSION['id_usuario']) {
            $this->errores = ['No es posible acceder a la información de otros usuarios.'];
        }

        if (empty($this->errores)) {
            $this->readDB($id);
        }
    }

    public function update(int $id, array $datos): void
    {
        if ($this->existeId($id)) {
            $this->errores = $this->validar(
                $datos['nick'],
                $datos['nick_actual'],
                'password_relleno', // para pasar la validación, luego el update no la toca
                $datos['nombre'],
                $datos['apellido1'],
                $datos['apellido2'],
                (int) $datos['edad'],
                $datos['email'],
                $datos['email_actual'],
                $datos['telefono']
            );
        } else {
            $this->errores = ['El \'ID\' no corresponde a ningún usuario'];
        }

        if (empty($this->errores)) {
            $this->updateDB(
                $id,
                $datos['nick'],
                $datos['nombre'],
                $datos['apellido1'],
                $datos['apellido2'],
                (int) $datos['edad'],
                $datos['email'],
                $datos['telefono']
            );
        }
    }

    public function delete(int $id): void
    {
        if ($this->existeId($id)) {
            null;
        } else {
            $this->errores[] = 'El \'ID\' no corresponde a ningún usuario';
        }

        if (empty($this->errores)) {
            $this->deleteDB($id);
        }
    }

    private function validar(string $nick, string $nick_actual, string $password, string $nombre, string $apellido1, string $apellido2, int $edad, string $email, string $email_actual, string $telefono): array
    {
        $errores = [];
        // mejorar con regex
        if (empty($nick)) {
            $errores[] = 'El campo \'nick\' es obligatorio';
        } elseif (strlen($nick) < MIN_USERNAME_LENGTH) {
            $errores[] = 'El nick debe tener una longitud mayor o igual que ' . MIN_USERNAME_LENGTH;
        } elseif ($nick != $nick_actual && $this->existeNick($nick)) {
            $errores[] = 'El nick ' . $nick . ' ya está en uso';
        }
        if (empty($password)) {
            $errores[] = 'El campo \'contraseña\' es obligatorio';
        } elseif (strlen($password) < MIN_USERNAME_LENGTH) {
            $errores[] = 'La contraseña debe tener una longitud mayor o igual que ' . MIN_PASSWORD_LENGTH;
        }
        if (empty($nombre)) {
            $errores[] = 'El campo \'nombre\' es obligatorio';
        }
        if (empty($email)) {
            $errores[] = 'El campo \'email\' es obligatorio';
        } elseif (!strpos($email, '@')) {
            $errores[] = 'El formato del email es erróneo';
        } elseif ($email != $email_actual && $this->existeEmail($email)) {
            $errores[] = 'El email ' . $email . ' ya está en uso';
        }
        if (empty($edad)) {
            $errores[] = 'El campo \'edad\' es obligatorio';
        } elseif ($edad < 0 || $edad > MAX_EDAD) {
            $errores[] = 'El valor de edad debe estar entre 0 y  ' . MAX_EDAD;
        }
        return $errores;
    }

    private function createDB(string $nick, string $password, string $nombre, string $apellido1, string $apellido2, int $edad, string $email, string $telefono): void
    {
        $sql = "INSERT INTO usuario (nick, password, nombre, apellido1, apellido2, edad, email, telefono, fecha_registro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, CURDATE())";
        $parametros = [$nick, $password, $nombre, $apellido1, $apellido2, $edad, $email, $telefono];
        if ($this->db->ejecutar($sql, $parametros)->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    private function readDB(int $id)
    {
        $sql = 'SELECT id, nick, nombre, apellido1, apellido2, edad, telefono, email, DATE_FORMAT(fecha_registro, "%e-%c-%Y") AS fecha_registro FROM usuario WHERE id = ?';
        $this->datos = $this->db->ejecutar($sql, [$id])->fetch();
    }

    private function updateDB(int $id, string $nick, string $nombre, string $apellido1, string $apellido2, int $edad, string $email, string $telefono): void
    {
        $sql = "UPDATE usuario SET nick=?, nombre=?, apellido1=?, apellido2=?, email=?, telefono=?, edad=? WHERE id=?";
        $parametros = [$nick, $nombre, $apellido1, $apellido2, $email, $telefono, $edad, $id];
        if ($this->db->ejecutar($sql, $parametros)->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    private function deleteDB(int $id)
    {
        if ($this->db->ejecutar('DELETE FROM usuario WHERE id = ?', [$id])->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    private function existeNick(string $nick): bool
    {
        if ($this->db->ejecutar("SELECT 1 FROM usuario WHERE nick = ?", [$nick])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    private function existeEmail(string $email): bool
    {
        if ($this->db->ejecutar("SELECT 1 FROM usuario WHERE email = ?", [$email])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    private function existeId(int $id): bool
    {
        if ($this->db->ejecutar("SELECT 1 FROM usuario WHERE id = ?", [$id])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    # Extras del perfil
    public function cambiarPassword(int $id, array $datos): void
    {
        if ($datos['pass_actual'] !== $this->db->ejecutar('SELECT password FROM usuario WHERE id = ?', [$id])->fetchColumn()) {
            $this->errores = ['Contraseña incorrecta'];
        } elseif ($datos['pass_nueva'] !== $datos['pass_confirm']) {
            $this->errores = ['Las contraseñas no coinciden'];
        } elseif (empty($datos['pass_nueva'])) {
            $this->errores = ['El campo \'contraseña\' es obligatorio'];
        } elseif (strlen($datos['pass_nueva']) < MIN_USERNAME_LENGTH) {
            $this->errores = ['La contraseña debe tener una longitud mayor o igual que ' . MIN_PASSWORD_LENGTH];
        } else {
            $sql = "UPDATE usuario SET password=? WHERE id=?";
            $parametros = [$datos['pass_nueva'], $id];
            if ($this->db->ejecutar($sql, $parametros)->rowCount()) {
                $this->accion_realizada = true;
            }
        }
    }

    public function baja(int $id, array $datos): void
    {
        if ($datos['pass_actual'] === $this->db->ejecutar('SELECT password FROM usuario WHERE id = ?', [$id])->fetchColumn()) {
            $this->delete($id);
        } else {
            $this->errores[] = 'Contraseña incorrecta';
        }
    }

    # Listable
    public function getLista(int $pagina = 1): void
    {
        $this->pagina = $pagina;
        $this->total_items = $this->db->ejecutar('SELECT COUNT(1) FROM mensaje')->fetchColumn();
        $this->inicio = ($pagina > 1) ? ($pagina * $this->items_pagina - $this->items_pagina) : 0;
        $this->numero_paginas = (int) ceil($this->total_items / $this->items_pagina);

        $sql = 'SELECT SQL_CALC_FOUND_ROWS id, nick
                FROM usuario ORDER BY id DESC
                LIMIT ?, ?';

        $this->datos = $this->db->ejecutar($sql, [$this->inicio, $this->items_pagina])->fetchAll();
    }

    # Getters
    public function getDatos(): array
    {
        return $this->datos;
    }

    public function getAccion(): bool
    {
        return $this->accion_realizada;
    }

    public function getErrores(): array
    {
        return $this->errores;
    }

    public function getPagina(): int
    {
        return $this->pagina;
    }

    public function getNumeroPaginas(): int
    {
        return $this->numero_paginas;
    }
}
