<?php

declare(strict_types=1);

namespace Models;

class Receta implements CRUD, Listable
{
    private \Core\DB $db;
    private array $datos;
    private bool $accion_realizada;
    private array $errores;
    private int $items_pagina;

    public function __construct(\Core\DB $db)
    {
        $this->db = $db;
        $this->datos = [];
        $this->accion_realizada = false;
        $this->errores = [];
        $this->items_pagina = 20;
    }

    # CRUD
    public function create(array $datos): void
    {
        $ingredientes = $this->arrayIngredientes($datos['ingredientes']);
        $this->errores = $this->validar(
            $datos['titulo'],
            $datos['elaboracion'],
            (int) $datos['tipo_receta'],
            $ingredientes
        );
        if (empty($this->errores)) {
            $this->createDB(
                $datos['titulo'],
                $datos['elaboracion'],
                (int) $datos['tipo_receta'],
                $ingredientes
            );
        }
    }

    public function read(int $id): void
    {
        if ($this->existeId($id)) {
            null;
        } else {
            $this->errores = ['El \'ID\' no corresponde a ninguna receta'];
        }

        if (empty($this->errores)) {
            $this->readDB($id);
        }
    }

    public function update(int $id, array $datos): void
    {
        error_log('update recibe ' . $datos['ingredientes']);
        $ingredientes = $this->arrayIngredientes($datos['ingredientes']);
        error_log('a array ' . implode('-', $ingredientes));
        if ($this->existeId($id)) {
            $this->errores = $this->validar(
                $datos['titulo'],
                $datos['elaboracion'],
                (int) $datos['tipo_receta'],
                $ingredientes,
                $datos['titulo_actual']
            );
        } else {
            $this->errores = ['El \'ID\' no corresponde a ninguna receta'];
        }
        if (empty($this->errores)) {
            error_log('update validado');
            $this->updateDB(
                $id,
                $datos['titulo'],
                $datos['elaboracion'],
                (int) $datos['tipo_receta'],
                $ingredientes
            );
        }
    }
    public function delete(int $id): void
    {
        if ($this->existeId($id)) {
            null;
        } else {
            $this->errores = ['El \'ID\' no corresponde a ninguna receta'];
        }

        if (empty($this->errores)) {
            $this->deleteDB($id);
        }
    }

    public function arrayIngredientes(string $string_ingredientes): array
    {
        $array_ingredientes = [];
        if (!empty($string_ingredientes)) {
            $array_ingredientes = explode(',', trim($string_ingredientes, ','));
        }
        return $array_ingredientes;
    }

    public function validar(string $titulo, string $elaboracion, int $id_tipo_receta, array $ingredientes, string $titulo_actual = null): array
    {
        $errores = [];
        // mejorar con regex
        if (empty($titulo)) {
            $errores[] = 'El campo \'titulo\' es obligatorio';
        } elseif ($titulo != $titulo_actual && $this->existeTitulo($titulo)) {
            $errores[] = 'El titulo ' . $titulo . ' ya está en uso';
        }
        if (empty($elaboracion)) {
            $errores[] = 'El campo \'elaboracion\' es obligatorio';
        }
        if (empty($id_tipo_receta)) {
            $errores[] = 'El campo \'tipo_receta\' es obligatorio';
        } elseif (!$this->tipoValido($id_tipo_receta)) {
            $errores[] = 'Tipo de receta no reconocido';
        }
        if (!$this->ingredientesValidos($ingredientes)) {
            $errores[] = 'Ingrediente no reconocido';
        }
        return $errores;
    }

    public function existeTitulo(string $titulo): bool
    {
        if ($this->db->ejecutar("SELECT 1 FROM receta WHERE titulo = ?", [$titulo])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    public function tipoValido(int $id_tipo_receta): bool
    {
        if ($this->db->ejecutar("SELECT 1 FROM tipo_receta WHERE id = ?", [$id_tipo_receta])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    public function ingredientesValidos(array $ingredientes): bool
    {
        $sql = "SELECT 1 FROM ingrediente WHERE id = ?";
        foreach ($ingredientes as $ingrediente) {
            if (!$this->db->ejecutar($sql, [(int) $ingrediente])->fetchColumn()) {
                return false;
            }
        }
        return true;
    }

    public function createDB(string $titulo, string $elaboracion, int $id_tipo_receta, array $ingredientes): void
    {
        $sql = "INSERT INTO receta (titulo, elaboracion, id_tipo_receta, id_admin) VALUES (?,?,?,?)";
        $parametros = [$titulo, $elaboracion, $id_tipo_receta, $_SESSION['id_admin']];
        if ($this->db->ejecutar($sql, $parametros)->rowCount()) {
            $this->accion_realizada = true;
            if (!empty($ingredientes)) {
                $id_receta = (int) $this->db->ultimoIdInsertado();
                $this->insertIngredientes($id_receta, $ingredientes);
            }
        }
    }

    public function insertIngredientes(int $id_receta, array $ingredientes)
    {
        error_log('insertando en receta ' . $id_receta . ' ingredientes ' . implode('-', $ingredientes));
        $sql = "INSERT INTO ingrediente_receta (id_ingrediente, id_receta) VALUES (?,?)";
        foreach ($ingredientes as $ingrediente) {
            error_log('insertando ' . $ingrediente);
            if ($this->db->ejecutar($sql, [(int) $ingrediente, $id_receta])->rowCount()) {
                error_log('insert ok');
            }
        }
    }

    public function readDB(int $id): void
    {
        $sql = 'SELECT
                    receta.id, titulo, elaboracion, DATE_FORMAT(fecha_publicacion, "%e-%c-%Y") AS fecha_publicacion,
                    id_admin, admin.nick as admin, id_tipo_receta, tipo_receta.nombre AS tipo_receta
                FROM receta
                LEFT JOIN tipo_receta ON receta.id_tipo_receta = tipo_receta.id
                LEFT JOIN admin ON receta.id_admin = admin.id
                WHERE receta.id = ?';

        $receta = $this->db->ejecutar($sql, [$id])->fetch();
        $receta['ingredientes'] = $this->getIngredientesReceta($id);
        $receta['comentarios'] = $this->getComentariosReceta($id);
        $receta['datos_valoracion'] = $this->getValoracionReceta($id);
        $this->datos = $receta;
    }

    public function getIngredientesReceta(int $id)
    {
        $sql = 'SELECT
                    ingrediente_receta.id_ingrediente, ingrediente.nombre, ingrediente_receta.cantidad,
                    ingrediente.id_tipo_ingrediente, tipo_ingrediente.nombre AS tipo_ingrediente
                FROM ingrediente_receta
                JOIN ingrediente ON ingrediente_receta.id_ingrediente = ingrediente.id
                LEFT JOIN tipo_ingrediente ON ingrediente.id_tipo_ingrediente = tipo_ingrediente.id
                WHERE id_receta = ?';

        return $this->db->ejecutar($sql, [$id])->fetchAll();
    }

    public function getComentariosReceta(int $id)
    {
        $sql = 'SELECT comentario.id AS id_comentario, usuario.nick AS usuario, contenido, fecha_hora_publicacion
                FROM comentario
                LEFT JOIN usuario ON comentario.id_usuario = usuario.id
                WHERE id_receta = ?
                ORDER BY fecha_hora_publicacion DESC';

        return $this->db->ejecutar($sql, [$id])->fetchAll();
    }

    public function getValoracionReceta(int $id)
    {
        $sql = 'SELECT COUNT(id_usuario) AS numero_valoraciones, AVG(nota) AS nota_media
                FROM valoracion
                WHERE id_receta = ?
                GROUP BY id_receta';

        return $this->db->ejecutar($sql, [$id])->fetch();
    }

    public function updateDB(int $id, string $titulo, string $elaboracion, int $id_tipo_receta, array $ingredientes): void
    {
        error_log('actualizando id ' . $id);
        error_log('actualizando titulo ' . $titulo);
        error_log('actualizando elaboracion ' . $elaboracion);
        error_log('actualizando id_tipo_receta ' . $id_tipo_receta);
        $sql = "UPDATE receta SET titulo = ?, elaboracion = ?, id_tipo_receta = ? WHERE id=?";
        $parametros = [$titulo, $elaboracion, $id_tipo_receta, $id];
        $this->db->ejecutar($sql, $parametros);
        error_log('update ejecutado');
        $this->accion_realizada = true;
        $rc = $this->db->ejecutar('DELETE FROM ingrediente_receta WHERE id_receta = ?', [$id])->rowCount();
        error_log('eliminados ' . $rc ?? 0 . ' ingredientes');

        if (!empty($ingredientes)) {
            error_log('hay ingredientes para insertar');
            $this->insertIngredientes($id, $ingredientes);
        }
    }
    public function deleteDB(int $id): void
    {
        if ($this->db->ejecutar('DELETE FROM receta WHERE id = ?', [$id])->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    public function existeId(int $id): bool
    {
        if ($this->db->ejecutar("SELECT 1 FROM receta WHERE id = ?", [$id])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    # Extras comentarios
    public function comentar(int $id, array $datos)
    {
        $this->errores = $this->validarComentario($id, $datos['contenido']);
        if (empty($this->errores)) {
            $this->createComentarioDB($id, $datos['contenido']);
        }
    }

    private function validarComentario(int $id_receta, string $contenido): array
    {
        $errores = [];
        if (!$this->existeId($id_receta)) {
            $errores[] = 'La receta no existe';
        }
        if (empty($contenido)) {
            $errores[] = 'El comentario debe tener \'contenido\'';
        }
        return $errores;
    }

    private function createComentarioDB(int $id_receta, string $contenido): void
    {
        $sql = "INSERT INTO comentario (id_receta, contenido, id_usuario) VALUES (?, ?, ?)";
        $parametros = [$id_receta, $contenido, $_SESSION['id_usuario']];

        if ($this->db->ejecutar($sql, $parametros)->rowCount()) {
            $this->accion_realizada = true;
            $this->datos['id_receta'] = $id_receta;
        }
    }

    private function deleteComentarioDB(int $id): void
    {   # de momento nadie puede borrar comentarios
        if ($this->db->ejecutar('DELETE FROM comentario WHERE id = ?', [$id])->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    public function existeComentarioId(int $id): bool
    {
        if ($this->db->ejecutar("SELECT 1 FROM comentario WHERE id = ?", [$id])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    public function valorar(int $id_receta, array $datos)
    {
        $this->errores = $this->validarValoracion($id_receta, (int) $datos['nota']);
        if (empty($this->errores)) {
            $this->createValoracionDB($id_receta, (int) $datos['nota']);
        }
    }

    private function validarValoracion(int $id_receta, int $nota): array
    {
        $errores = [];
        if (!$this->existeId($id_receta)) {
            $errores[] = 'La receta no existe';
        }
        if (empty($nota)) {
            $errores[] = 'No se ha recibido la valoración';
        } elseif ($nota < 1 || $nota > 5) {
            $errores[] = 'La nota debe estar entre 1 y 5';
        }
        return $errores;
    }

    private function createValoracionDB(int $id_receta, int $nota): void
    {
        if ($this->existeValoracionUser($id_receta, $_SESSION['id_usuario'])) {
            $sql = "UPDATE valoracion SET nota = ? WHERE id_receta = ? AND id_usuario = ?";
            $parametros = [$nota, $id_receta, $_SESSION['id_usuario']];
        } else {
            $sql = "INSERT INTO valoracion (id_receta, nota, id_usuario) VALUES (?, ?, ?)";
            $parametros = [$id_receta, $nota, $_SESSION['id_usuario']];
        }
        $this->db->ejecutar($sql, $parametros)->rowCount();
        $this->accion_realizada = true;
        $this->datos['id_receta'] = $id_receta;
    }

    private function existeValoracionUser(int $id_receta, int $id_usuario)
    {
        if ($this->db->ejecutar("SELECT 1 FROM valoracion WHERE id_receta = ? AND id_usuario = ?", [$id_receta, $id_usuario])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    # Edit Form
    public function getTiposRecetas(): array
    {
        return $this->db->ejecutar('SELECT id, nombre FROM tipo_receta')->fetchAll();
    }

    public function getIngredientesRecetas(): array
    {
        $sql = 'SELECT ingrediente.id, ingrediente.nombre, ingrediente.id_tipo_ingrediente, tipo_ingrediente.nombre AS tipo_ingrediente
                FROM ingrediente
                JOIN tipo_ingrediente ON ingrediente.id_tipo_ingrediente = tipo_ingrediente.id';
        return $this->db->ejecutar($sql)->fetchAll();
    }



    # Listable
    public function getLista(int $pagina = 1): void
    {
        $this->pagina = $pagina;
        $this->total_items = $this->db->ejecutar('SELECT COUNT(1) FROM mensaje')->fetchColumn();
        $this->inicio = ($pagina > 1) ? ($pagina * $this->items_pagina - $this->items_pagina) : 0;
        $this->numero_paginas = (int) ceil($this->total_items / $this->items_pagina);

        $sql = "SELECT SQL_CALC_FOUND_ROWS receta.id, titulo, DATE_FORMAT(fecha_publicacion, '%e-%c-%Y') as fecha_publicacion, nick AS admin
                FROM receta JOIN admin ON receta.id_admin = admin.id
                ORDER BY fecha_publicacion DESC
                LIMIT ?, ?";

        $this->datos = $this->db->ejecutar($sql, [$this->inicio, $this->items_pagina])->fetchAll();
    }

    # Getters
    public function getDatos(): array
    {
        return $this->datos;
    }

    public function getAccion(): bool
    {
        return $this->accion_realizada;
    }

    public function getErrores(): array
    {
        return $this->errores;
    }
    
    public function getPagina(): int
    {
        return $this->pagina;
    }

    public function getNumeroPaginas(): int
    {
        return $this->numero_paginas;
    }
}
