<?php

namespace Models;

interface Listable
{
    public function getLista(int $pagina): void;
}