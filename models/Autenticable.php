<?php

namespace Models;

interface Autenticable
{
    public function setLogin(array $credenciales): void;
    public function getLogin(): bool;
    public function getIntentado(): bool;
    public function endLogin(): void;
}
