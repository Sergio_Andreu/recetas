<?php

declare(strict_types=1);

namespace Models;

class Login implements Autenticable
{
    private \Core\DB $db;
    private bool $login_intentado;

    public function __construct(\Core\DB $db)
    {
        $this->db = $db;
        $this->login_intentado = false;
    }

    public function setLogin(array $credenciales): void
    {
        $sql = 'SELECT id FROM usuario WHERE nick = ? AND password = ?';
        if ($id = $this->db->ejecutar($sql, [$credenciales['nick'], $credenciales['password']])->fetchColumn()) {
            $_SESSION['login_usuario'] = true;
            $_SESSION['usuario'] = $credenciales['nick'];
            $_SESSION['id_usuario'] = $id;
        } else {
            $this->login_intentado = true;
        }
    }

    public function getLogin(): bool
    {
        if (isset($_SESSION['login_usuario']) && $_SESSION['login_usuario'] === true) {
            return true;
        } else {
            return false;
        }
    }

    public function getIntentado(): bool
    {
        return $this->login_intentado;
    }

    public function endLogin(): void
    {
        session_unset();
        session_destroy();
    }
}
