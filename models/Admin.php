<?php

declare(strict_types=1);

namespace Models;

class Admin implements Crud
{
    private \Core\DB $db;
    private array $datos;
    private bool $accion_realizada;
    private array $errores;

    public function __construct(\Core\DB $db)
    {
        $this->db = $db;
        $this->datos = [];
        $this->accion_realizada = false;
        $this->errores = [];
    }

    # CRUD
    public function create(array $datos): void
    {
        $this->errores = $this->validar(
            $datos['nick'],
            $datos['password']
        );

        if (empty($this->errores)) {
            $this->insertDB(
                $datos['nick'],
                $datos['password']
            );
        }
    }

    public function read(int $id): void
    {
        // $sql = 'SELECT nick, nombre, DATE_FORMAT(fecha_registro, "%e-%c-%Y") AS fecha_registro FROM usuario WHERE id = ?';
        // $this->datos = $this->db->ejecutar($sql, [$id])->fetch();
        null; # no se usa
    }
    public function update(int $id, array $datos): void
    {
        null; # no se usa
    }
    public function delete(int $id): void
    {
        // if ($this->db->ejecutar('DELETE FROM admin WHERE id = ?', [$id])->rowCount()) {
        //     $this->accion_realizada = true;
        // }
        null; # No se usa
    }

    private function validar(string $nick, string $password): array
    {
        $errores = [];
        // mejorar con regex
        if (empty($nick)) {
            $errores[] = 'El campo \'nick\' es obligatorio';
        } elseif (strlen($nick) < MIN_USERNAME_LENGTH) {
            $errores[] = 'El nick debe tener una longitud mayor o igual que ' . MIN_USERNAME_LENGTH;
        } elseif ($this->existeNick($nick)) {
            $errores[] = 'El nick ' . $nick . ' ya está en uso';
        }
        if (empty($password)) {
            $errores[] = 'El campo \'contraseña\' es obligatorio';
        } elseif (strlen($password) < MIN_USERNAME_LENGTH) {
            $errores[] = 'La contraseña debe tener una longitud mayor o igual que ' . MIN_PASSWORD_LENGTH;
        }
        return $errores;
    }

    public function insertDB(string $nick, string $password): void
    {
        $sql = "INSERT INTO admin (nick, password, fecha_registro) VALUES (?, ?, CURDATE())";
        $parametros = [$nick, $password];
        if ($this->db->ejecutar($sql, $parametros)->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    public function existeNick(): bool
    {
        if ($this->db->ejecutar("SELECT 1 FROM admin WHERE nick = ?", [$this->nick])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    # Getters
    public function getDatos(): array
    {
        return $this->datos;
    }

    public function getAccion(): bool
    {
        return $this->accion_realizada;
    }

    public function getErrores(): array
    {
        return $this->errores;
    }
}
