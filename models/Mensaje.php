<?php

declare(strict_types=1);

namespace Models;

class Mensaje implements CRUD, Listable
{
    private \Core\DB $db;
    private array $datos;
    private bool $accion_realizada;
    private array $errores;
    private int $items_pagina;

    public function __construct(\Core\DB $db)
    {
        $this->db = $db;
        $this->datos = [];
        $this->accion_realizada = false;
        $this->errores = [];
        $this->items_pagina = 20;
    }

    # CRUD
    public function create(array $datos): void
    {
        $this->errores = $this->validar($datos['asunto'], $datos['contenido']);
        if (empty($this->errores)) {
            $this->createDB($datos['asunto'], $datos['contenido']);
        }
    }

    public function read(int $id): void
    {
        if ($this->existeId($id)) {
            null;
        } else {
            $this->errores = ['El \'ID\' no corresponde a ningún mensaje'];
        }

        if (empty($this->errores)) {
            $this->readDB($id);
            $this->updateDB($id);
        }
    }

    public function update(int $id, array $datos): void
    {
        null; # No se usa
    }

    public function delete(int $id): void
    {
        if ($this->existeId($id)) {
            null;
        } else {
            $this->errores = ['El \'ID\' no corresponde a ningún mensaje'];
        }

        if (empty($this->errores)) {
            $this->deleteDB($id);
        }
    }

    private function validar(string $asunto, string $contenido): array
    {
        $errores = [];
        if (empty($asunto)) {
            $errores[] = 'El mensaje debe tener \'asunto\'';
        }
        if (empty($contenido)) {
            $errores[] = 'El mensaje debe tener \'contenido\'';
        }
        return $errores;
    }

    private function createDB(string $asunto, string $contenido): void
    {
        $sql = "INSERT INTO mensaje (asunto, contenido, id_usuario) VALUES (?, ?, ?)";
        $parametros = [$asunto, $contenido, $_SESSION['id_usuario']];
        // Acceso global a $_SESSION que me gustaría controlar, de momento se queda
        // pero tendría que pasarlo en $datos al método create
        if ($this->db->ejecutar($sql, $parametros)->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    private function readDB(int $id): void
    {
        $sql = 'SELECT mensaje.*, usuario.nick FROM mensaje JOIN usuario ON mensaje.id_usuario = usuario.id WHERE mensaje.id = ?';
        $this->datos = $this->db->ejecutar($sql, [$id])->fetch();
    }

    private function updateDB(int $id): void
    {
        if ($this->db->ejecutar("UPDATE mensaje SET leido = 1 WHERE id = ?", [$id])->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    private function deleteDB(int $id): void
    {
        if ($this->db->ejecutar('DELETE FROM mensaje WHERE id = ?', [$id])->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    public function existeId(int $id): bool
    {
        if ($this->db->ejecutar("SELECT 1 FROM mensaje WHERE id = ?", [$id])->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    # Listable
    public function getLista(int $pagina = 1): void
    {
        $this->pagina = $pagina;
        $this->total_items = $this->db->ejecutar('SELECT COUNT(1) FROM mensaje')->fetchColumn();
        $this->inicio = ($pagina > 1) ? ($pagina * $this->items_pagina - $this->items_pagina) : 0;
        $this->numero_paginas = (int) ceil($this->total_items / $this->items_pagina);

        $sql = 'SELECT SQL_CALC_FOUND_ROWS mensaje.*, usuario.nick
                FROM mensaje JOIN usuario ON mensaje.id_usuario = usuario.id
                ORDER BY fecha_hora_enviado DESC
                LIMIT ?, ?';

        $this->datos = $this->db->ejecutar($sql, [$this->inicio, $this->items_pagina])->fetchAll();
    }

    # Getters
    public function getDatos(): false|array
    {
        return $this->datos;
    }

    public function getErrores(): array
    {
        return $this->errores;
    }

    public function getAccion(): bool
    {
        return $this->accion_realizada;
    }

    public function getPagina(): int
    {
        return $this->pagina;
    }

    public function getNumeroPaginas(): int
    {
        return $this->numero_paginas;
    }
}
